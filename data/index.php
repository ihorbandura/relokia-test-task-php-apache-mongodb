<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	require_once __DIR__ . "/vendor/autoload.php";

	// Get JSON
	try
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, 'http://resources.finance.ua/ua/public/currency-cash.json');
		$JSONResult = curl_exec($ch);
		curl_close($ch);
	} catch(Exception $e) {
		echo 'Error While Getting JSON From URL <br /> Exception: '.$e->getMessage();
		return;
	}

	if(is_null($JSONResult)) {
		throw "Error: Fetched JSON is not valid or empty.";
		return;
	}

	// Decode JSON string To PHP Object
	try
	{
		$currencyCash = json_decode($JSONResult);
	} catch(Exception $e) {
		echo 'Error While Decoding JSON <br /> Exception: '.$e->getMessage();
		return;
	}

	if(is_null($currencyCash)) {
		throw "Error: Decoded JSON is not valid or empty.";
		return;
	}

	// Save Data To DB
	try
	{
		$con = new  MongoDB\Client("mongodb://my-mongo-container:27017/?compressors=disabled&gssapiServiceName=mongodb");
		$financedb = $con->financedb;

		$dataFetchHistoryID = $financedb->financeDataFetchHistory->insertOne( [ 'date' => $currencyCash->date, 'sourceid' => $currencyCash->sourceId, 'date_fetched' => date('m/d/Y h:i:s a', time()) ])->getInsertedId();

		if(!is_null($currencyCash->cities)) {
			$dbCol = $financedb->cities;
			foreach($currencyCash->cities as $id => $obj) {
				if(is_null($dbCol->findOne( [ 'id' => $id ] )))
					$dbCol->insertOne( [ 'id' => $id, 'city' => $obj, 'dataFetchHistoryID' => $dataFetchHistoryID ] );
			}
		}

		if(!is_null($currencyCash->regions)) {
			$dbCol = $financedb->regions;
			foreach($currencyCash->regions as $id => $obj) {
				if(is_null($dbCol->findOne( [ 'id' => $id ] )))
					$dbCol->insertOne( [ 'id' => $id, 'region' => $obj, 'dataFetchHistoryID' => $dataFetchHistoryID ] );
			}
		}

		if(!is_null($currencyCash->currencies)) {
			$dbCol = $financedb->currencies;
			foreach($currencyCash->currencies as $id => $obj) {
				if(is_null($dbCol->findOne( [ 'id' => $id ] )))
					$dbCol->insertOne( [ 'id' => $id, 'currency' => $obj, 'dataFetchHistoryID' => $dataFetchHistoryID ] );
			}
		}

		if(!is_null($currencyCash->orgTypes)) {
			$dbCol = $financedb->organizationTypes;
			foreach($currencyCash->orgTypes as $id => $obj) {
				if(is_null($dbCol->findOne( [ 'id' => $id ] )))
					$dbCol->insertOne( [ 'id' => $id, 'organizationType' => $obj, 'dataFetchHistoryID' => $dataFetchHistoryID ] );
			}
		}

		if(!is_null($currencyCash->organizations)) {
			foreach($currencyCash->organizations as $organization) {
				if(!is_null($organization)) {
					$organization->dataFetchHistoryID = $dataFetchHistoryID;
					$financedb->organizations->insertOne($organization);
				}
			}
		}
	} catch(Exception $e) {
		echo 'Error occured while saving data. <br /> Exception: '.$e->getMessage();
		return;
	}
?>